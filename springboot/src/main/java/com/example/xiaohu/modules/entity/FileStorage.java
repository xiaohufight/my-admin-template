package com.example.xiaohu.modules.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@TableName("FILE_STORAGE")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class FileStorage implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    private String fileName;

    private String fileUrl;

    private String uploadUsername;

    private String uploadTime;

}
