package com.example.xiaohu.modules.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.xiaohu.modules.entity.FileStorage;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface FileStorageMapper extends BaseMapper<FileStorage> {
}
