package com.example.xiaohu.modules.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.xiaohu.modules.entity.FileStorage;
import org.springframework.web.multipart.MultipartFile;

public interface FileStorageService extends IService<FileStorage> {
    String uploadFile(MultipartFile file,String uid);

    void deleteMinioFile(FileStorage fileStorage);
}
