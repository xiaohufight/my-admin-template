-- 2021-11-01
-- xiaohu
-- 学习练手代码，仅供参考，欢迎交流

CREATE database IF NOT EXISTS `myAdminTemplate` DEFAULT CHARSET=utf8;
USE `myAdminTemplate`;

DROP TABLE IF EXISTS `SYS_USER`;
CREATE TABLE `SYS_USER`(
	`ID` varchar(32) NOT NULL,
	`USERNAME` varchar(255) NOT NULL,
	`PASSWORD` varchar(255) NOT NULL,
	`SALT` varchar(255) NOT NULL,
	`STATUS` varchar(2) NOT NULL,
	`CREATE_TIME` DATETIME NOT NULL,
	 PRIMARY KEY(`ID`)
)ENGINE = InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `SYSTEM_INFO`;
CREATE TABLE `SYSTEM_INFO` (
  `ID` varchar(32) NOT NULL,
  `HOST_NAME` varchar(64) DEFAULT NULL,
  `OS_NAME` varchar(64) DEFAULT NULL,
  `VERSION` varchar(64) DEFAULT NULL,
  `CPU_CORE_NUM` varchar(10) DEFAULT NULL,
  `CREATE_TIME` timestamp NULL DEFAULT NULL,
  `STATE` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `SYS_ROLE`;
CREATE TABLE `SYS_ROLE`(
  `ID` varchar(32) NOT NULL,
  `ROLE_NAME` varchar(32) NOT NULL,
  `DESCRIPTION` varchar(1024),
  PRIMARY KEY(`ID`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `SYS_USER_ROLE`;
CREATE TABLE `SYS_USER_ROLE`(
  `ID` varchar(32) NOT NULL,
  `USER_ID` varchar(32) NOT NULL,
  `ROLE_ID` varchar(32) NOT NULL,
  PRIMARY KEY(`ID`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `FILE_STORAGE`;
CREATE TABLE `FILE_STORAGE`(
	`ID` varchar(32) NOT NULL,
	`FILE_NAME` varchar(128) NOT NULL,
	`FILE_URL` varchar(256) NOT NULL,
	`UPLOAD_USERNAME` varchar(16),
	`UPLOAD_TIME` varchar(32) NOT NULL,
	 PRIMARY KEY(`ID`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

set global log_bin_trust_function_creators=TRUE;

-- 生成随机salt
DROP FUNCTION IF EXISTS getSalt;
DELIMITER $$
CREATE FUNCTION getSalt(n INT)
RETURNS VARCHAR(255)
BEGIN
    DECLARE base varchar(100) DEFAULT 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    DECLARE salt varchar(255) DEFAULT '';
    DECLARE i INT DEFAULT 0;
    WHILE i < n DO
        SET salt = concat(salt,substring(base , FLOOR(1 + RAND()*62 ),1));
        SET i = i +1;
    END WHILE;
    RETURN salt;
END  $$
DELIMITER ;

-- 创建用户
SET @salt = getSalt(8);
INSERT INTO SYS_USER VALUES(REPLACE(UUID(), '-', ''),"admin",md5(concat("123456",@salt)),@salt,'0',NOW());
SET @salt = getSalt(8);
INSERT INTO SYS_USER VALUES(REPLACE(UUID(), '-', ''),"xiaoming",md5(concat("123456",@salt)),@salt,'0',NOW());

-- 创建角色
INSERT INTO SYS_ROLE VALUES(REPLACE(UUID(), '-', ''),'admin','管理员');
INSERT INTO SYS_ROLE VALUES(REPLACE(UUID(), '-', ''),'user','普通用户');

-- 设置用户角色
INSERT SYS_USER_ROLE SELECT REPLACE(UUID(), '-', '') id ,su.id user_id, sr.id role_id FROM SYS_USER su join SYS_ROLE sr WHERE su.username = 'admin' and sr.role_name = 'admin';
INSERT SYS_USER_ROLE SELECT REPLACE(UUID(), '-', '') id ,su.id user_id, sr.id role_id FROM SYS_USER su join SYS_ROLE sr WHERE su.username = 'xiaoming' and sr.role_name = 'user';