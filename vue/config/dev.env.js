'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  API_ROOT:'"http://localhost:7000/myAdminTemplate"',
  MUSIC_API_ROOT:'"https://netease-cloud-music-api-livid-eight.vercel.app"'
})
