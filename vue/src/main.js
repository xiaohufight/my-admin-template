// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import Storage from 'vue-ls'
import '@/permission.js'
import Antd from 'ant-design-vue'
import 'ant-design-vue/dist/antd.css'
import * as echarts from 'echarts'
import moment from 'moment'
import mavonEditor from 'mavon-editor'
import 'mavon-editor/dist/css/index.css'
import {ACCESS_TOKEN, USER_NAME} from "./store/mutation-types"
import VueCookies from 'vue-cookies'
import VueAudio from 'vue-audio-better'

Vue.use(VueAudio)
Vue.use(Antd)
Vue.use(Storage)
Vue.use(mavonEditor)
Vue.use(VueCookies)

Vue.prototype.$echarts = echarts
Vue.prototype.$moment = moment
Vue.config.productionTip = false
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  mounted(){
    store.commit('SET_TOKEN',Vue.ls.get(ACCESS_TOKEN))
  },
  components: { App },
  template: '<App/>'
})
