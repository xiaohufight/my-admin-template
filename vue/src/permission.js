import Vue from 'vue'
import router, {asynacRouterMap} from './router'
import store from './store'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
import {ACCESS_TOKEN, ROLE, USER_NAME} from "./store/mutation-types"

NProgress.configure({ showSpinner: false });
const whiteList = ['/register','/'];
router.beforeEach((to,from,next)=>{
  NProgress.start();
  if(Vue.ls.get(ACCESS_TOKEN)){
    if(to.path==='/'){
      next('/home');
      NProgress.done();
    }else{
      if(store.getters.role.length===0){
        store.dispatch("getInfo").then(res=>{
          store.dispatch('generateRoutes',store.getters.role).then(accessRoutes=>{
            router.addRoutes(accessRoutes);
            next({...to,replace:true})
          });
        });

      }else{
        next()
      }
    }
  }else{
    if(whiteList.indexOf(to.path)!==-1){
      next()
    }else{
      next({path:'/'})
    }
  }
})


router.afterEach(() => {
  NProgress.done();
})
