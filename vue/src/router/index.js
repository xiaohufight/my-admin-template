import Vue from 'vue'
import Router from 'vue-router'

import Login from '@/views/login/Login.vue'

import Register from '@/views/register/Register.vue'
import Layout from "../components/layout/Layout";
import Home from "../views/home/Home";
import SystemInfo from "../views/system/SystemInfo";
import MarkDown from "../views/editTool/MarkDown";
import RoleManage from "../views/systemSetting/RoleManage";
import SysUserManage from "../views/systemSetting/SysUserManage";
import MenuManage from "../views/systemSetting/MenuManage";
import FilePreviewOnline from "../views/filePreviewOnline/FilePreviewOnline"

Vue.use(Router)


//NavigationDuplicated: Avoided redundant navigation to current location:
//暂未想到别的解决方法
const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

export const constantRouterMap = [
  {
    path: '/',
    name: 'login',
    component: Login
  },
  {
    path:'/register',
    name:'register',
    component:Register
  },
  {
    path:'/home',
    name:'layout',
    component: Layout,
    children:[
      {
        path:'/home',
        name:'home',
        component: Home,
        meta:{
          title:'首页',
          icon:'home',
          roles:['admin','user']
        }
      },
      {
        path:'/systemInfo',
        name:'systemInfo',
        component: SystemInfo,
        meta:{
          title:'系统信息',
          icon:'dashboard',
          roles:['admin','user']
        }
      },
      {
        path:'/markdown',
        name:'markdown',
        component:MarkDown,
        meta:{
          title:'Markdown',
          icon:'file-markdown',
          roles:['admin','user']
        }
      },
      {
        path:'/file-preview-online',
        name:'file-preview-online',
        component:FilePreviewOnline,
        meta:{
          title:'文件在线预览',
          icon:'file',
          roles:['admin','user']
        }
      }
    ]
  }
]
export const asynacRouterMap = [
  {
    path:'/home',
    name:'layout',
    component: Layout,
    children:[
    {
      path:'/sysUserManage',
      name:'sysUserManage',
      component:SysUserManage,
      meta: {
        roles:['admin'],
        title:'用户管理',
        icon:'team'
      }
    },
    {
      path:'/roleManage',
      name:'roleManage',
      component:RoleManage,
      meta:{
        roles:['admin'],
        title:'角色管理',
        icon:'user'
      }
    },
    {
      path:'/MenuManage',
      name:'MenuManage',
      component:MenuManage,
      meta:{
        roles:['admin'],
        title:'菜单管理`',
        icon:'setting'
      }
    }
    ]
  }
]


export default new Router({
  mode:'history',
  routes: constantRouterMap
})
