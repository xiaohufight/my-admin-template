import {ROUTE, USER_NAME} from "./mutation-types";
import Vue from 'vue';


const getters = {
  token: state => state.user.token,
  username: state => {state.user.username=Vue.ls.get(USER_NAME);return state.user.username;},
  role: state => state.user.role,
  route: state => state.permission.route
}

export default  getters;





