import {asynacRouterMap,constantRouterMap} from "../router"
import Vue from 'vue'
import {ROUTE} from "./mutation-types";

//这里照搬了花裤衩大佬的permission.js，并删减了一些
function hasPermission(role,route){
  if(route.meta && route.meta.roles){
    return role.some(role=>route.meta.roles.includes(role));
  }else{
    return true;
  }
}

//根据角色，递归生成动态路由
export function filterAsyncRoutes(routes,role){
  const res = [];
  routes.forEach(route =>{
    const tmp = {...route};     //深拷贝一个对象
    if(hasPermission(role,tmp)){
      if(tmp.children){
        tmp.children = filterAsyncRoutes(tmp.children,role);
      }
      res.push(tmp);
    }
  })
  return res;
}
const permission = {
  state:{
    routes:[],
    addRoutes:[]
  },
  mutations:{
    SET_ROUTES:(state, routes) =>{
      state.addRoutes = routes;
      state.routes = constantRouterMap.concat(routes);
      Vue.ls.set(ROUTE,state.routes,30 * 60 * 1000*10);
    }
  },
  actions:{
    generateRoutes({ commit },role){
      return new Promise(resolve=>{
        let accessRoutes = filterAsyncRoutes(asynacRouterMap,role);
        commit('SET_ROUTES',accessRoutes);
        resolve(accessRoutes);
      })
    }
  }
}


export default permission;
