//TODO 准备重写  代码比较混乱，没有模块化

import {getAction, postAction} from "../api/manage";
import {ACCESS_TOKEN, ROLE} from "./mutation-types";
import Vue from 'vue'

const user = {
  state:{
    username:'',
    token:'',
    role:[],
    info:{}
  },
  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_NAME: (state, username) => {
      state.username = username
    },
    SET_ROLE: (state, role) => {
      state.role = role
      Vue.ls.set(ROLE,role,30 * 60 * 1000*10)
    }
  },
  actions:{
    //获取用户信息
    getInfo({ commit }){
        return new Promise((resolve,reject)=>{
          getAction("/sys/getRoleByToken").then((res)=>{
            const {data} = res
            commit('SET_ROLE',[data])
            resolve([data])
          }).catch(error=>{
            reject(error)
          })
        })

    }
  }
}

export default user;
